---
title: Hudson Middle School Wins 1st and 2nd at Akron Regional!
date: 2023-03-14 21:36:00 Z
layout: post
---

Congratulations to the science olympiad team for winning 1st and 2nd place at the Akron Regionals! The team competed at the Akron Regionals on Saturday March 11 and took home a whopping 22 first place medals and a number of second and third place medals.

The team has been working hard since october, meeting up every Monday to pratice and study for their tests. And a shoutout to the build team that has been meeting up almost every day and working hard on their builds. And a big thank you to our coaches, Coach Renna and Coach Patel, as well as too all of the event mentors adults that have helped us get to where we are!

1st place: Anatomy & Physiology (Emily Shott/Gemma Ward)

1st place: Bio Process Lab (Emily Shott/Gemma Ward) 

1st place: Bridge (Evan Lee/Maanav Patel)

1st place: Can’t Judge a Powder (Romey Kim/Sherry Li) 

1st place: Code Busters (Maanav Patel/Sherry Li/Steven Fodor)

1st place: Crave the Wave (Sherry Li/Steven Fodor)

1st place: Crime Busters (Jocelyn Vogel/Sherry Li)

1st place: Disease Detectives (Elena Cheng/Romey Kim)

1st place: Dynamic Planet (Elana Cheng/Gemma Ward)

1st place: Experimental Design (Evan Lee/Kate Justice/Maanav Patel)

1st place: Fast Facts (Anna Barger/Emily Shott)

1st place: Flight (Evan Lee/Maanav Patel)

1st place: Forestry (Gemma Ward/Millie Griffiths)

1st place: Green Generation (Anna Barger/Emily Shott)

1st place: Meteorology (Elena Cheng/Gemma Ward)

1st place: Road Scholar (Julia Schneider/Millie Griffiths)

1st place: Roller Coaster (Hajoo Yu/Kate Justice)

1st place: Solar System (Anna Barger/Elena Cheng)

1st place: Sounds of Music (Evan Lee/Sherry Li)

1st place: Storm the Castle (Jocelyn Vogel/Matthew Matyja)

1st place: Wheeled Vehicle (Hajoo Yu/Kate Justice)

1st place: Write It/Do It (Jocelyn Vogel/Matthew Matyja)

2nd place: Bridge (Mariam Khankan/Talia Zmeili)

2nd place: Code Busters (Evan Peng/Hudson Reaper/Rithi Hegde)

2nd Place: Crave the Wave (Evan Peng/Jane Winston)

2nd place: Disease Detectives (Maria Metoki/Yusuke Takatori)

2nd place: Experimental Design (Hudson Reaper/Jax Murgatroyd/Morgan Griffiths)

2nd place: Rocks and Minerals (Julia Schneider/Millie Griffiths)

2nd place: Roller Coaster (Andrew Yamokoski/William Pendergrass)

2nd place: Sounds of Music (Hudson Reaper/Maria Metoki)

2nd place: Storm the Castle (Jax Murgatroyd/William Pendergrass)

3rd place: Bio Process Lab (Maria Metoki/Talia Zmeili)

3rd place: Fast Facts (Hudson Reaper/William Pendergrass)

3rd place: Flight (Elena Varga/Evie Jensen)

3rd place: Green Generation (Talia Zmeili/Yusuke Takatori)

3rd place: Solar System (Evie Jensen/Jane Winston)

4th place: Anatomy & Physiology (Rithi Hegde/BY HERSELF!!!!!)

4th place: Crime Busters (Evan Peng/Rithi Hegde)

4th place: Dynamic Planet (Jane Winston/Evan Peng)

4th place: Forestry (Liam Ward/Yusuke Takatori)

4th place: Rocks and Minerals (Elena Varga/Yusuke Takatori)

4th place: Wheeled Vehicle (Mariam Khankan/Talia Zmeili)

6th place: Can’t Judge a Powder (Liam Ward/Rithi Hegde)

6th place: Road Scholar (Morgan Griffiths/Jax Murgatroyd)