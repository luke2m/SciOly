---
title: Contact us
position: 2
description: Contact Dr. Renna, the head coach, or the webmaster.
layout: default
---

If you have questions about HMS Science Olympiad, please contact Dr. Renna.
[hmsscienceolympiad@hudson.k12.oh.u](mailto:hmsscienceolympiad@hudson.k12.oh.u)


<p> If you have feedback or problems with this website, please fill out the form below.
<hr>
<br>
<form id="contact" name="contact" netlify>
<fieldset>
<label for="name">Name
</label>
<br>
<input type="text" id="name" name="name">
<br>
<label for="email"> Email </label>
<br>
<input type="email" id="email" name="email">
<br>
<br>
<p> Type your question or feedback here. </p>
<textarea name="comment" form="contact">
</textarea>
<input type="submit" value="Submit">
</fieldset>
</form>