---
title: HMS SciOly Resources
position: 1
layout: default
---

## Links
***
* [Official Science Olympiad website](https://soinc.org)

* [SciOly wiki and forums](https://soinc.org)

* [HMS Shared drive](https://soinc.org)

## iPad resources

---

The iPad does not come with a calculator, and the one in Self Service is full of ads. To install these, in Safari, press share, then Add to Home Screen.

* [Four Function](https://pwa.llumino.app)

* [Four Function](https://chrisdiana.dev/pwa-calculator/index.html)

* [Scientific](https://bhar.app/calculator)

* [Scientific](https://multicalculator.app)

Periodic Table

* [Atom](https://atom.horuslugo.com)