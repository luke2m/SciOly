---
title: The 2021-22 Invitational season
date: 2022-03-23 00:00:00 Z
layout: post
---

The invitational season this year came with a lot of success for the Hudson Middle School Science Olympiad team. We steadily improved as a team over the season, and are ready to begin our official competition at the Akron Regional on March 12th.
We started the season at the Dodgen-Walton invitational with many of the top teams in the US, where we placed 28th, 37th, and 48th out of 65 teams.

We next took 5th place at the Dick Smith Memorial tournament.  Our Silver team took 14th place and our White team took 25th place out of 52 teams, including many state champions and teams that were at the US National Tournament last year. In total, we took home twenty-two sets of medals!  A big congratulations to Sherry Li and Jerry Du who took 1st place in Crimebusters and 2nd place in Crave the Wave!

Our team then had two invitationals on the same day, which was the first time Hudson Middle School has ever done that (and hopefully the last). We placed 5th, 19th and 20th in the Westlake Invitational, with a first place finish in Electric Wright Stuff by Yeonwoo and Hajoo Yu. The Cal-Tech Invitational on the same day was the largest Science Olympiad tournament ever, with over 450 middle and high school teams participating. Overall, our team placed 15th!

The next tournament we competed in was the Solon Invitational. There were 81 teams from all over the USA, and we placed 4th as a team. In the Storm The Castle event, all three teams were on the leaderboard, placing 2nd, 6th, and 7th.

The following tournament we took place in was the Centerville Invitational. There were 96 teams competing there, and we placed 13th, 27th, and 37th. A big congratulations to all the medal winners, including 1st place in Ornithology(Gemma Ward/Millie Griffiths).

Our last invitational we took place in this year was the Mentor Invitational. It was a very exciting tournament, as it was the first in person tournament the HMS Science Olympiad team has attended in nearly 2 years. Hudson placed 4th, 9th, and 11th, and brought home 37 sets of medals. This included six first place medals, which were Ornithology, Mousetrap Vehicle, Road Scholar, Memeology, Experimental Design, and Storm the Castle. All of those medals were made possible by parents and mentors, who also helped grade the Experimental Design and Storm the Castle events.

The HMS Science Olympiad team is excited about the Akron Regional, and are hoping to achieve many medals and continue to the states and nationals tournaments.

<script src="https://cdn.jsdelivr.net/npm/canvas-confetti@1.3.2/dist/confetti.browser.min.js"></script>
<script>
confetti({
particleCount: 100,
spread: 70,
origin: { y: 0.6 }
});
</script>