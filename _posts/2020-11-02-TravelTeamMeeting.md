---
title: Travel team meeting
date: 2020-11-02 00:00:00 Z
layout: post
author: Luke
---

Just a reminder that there will be a Zoom meeting for all new and returning members from 6 to 7 pm today, November 2nd. Check your email for the link.
